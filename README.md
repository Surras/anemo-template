# anemo-template

Angular - NestJS - MongoDB Template

After cloning the project, you need to rename it.

go to:
- frontend/package.json
    - change "name"-property to desired project name
- frontend/angular.json
    - rename all occurrences of "anemo-template" to your desired project name
    - run npm install (if you already run the command, please delete node_modules directory first)

- backend/package.json
    - change "name"-property to desired project name

## deployment

Do not bundle NestJS Server with dependencies. Read this article to find out why: https://github.com/ZenSoftware/bundled-nest
